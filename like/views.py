from django.shortcuts import render, redirect, reverse
from django.http import JsonResponse
from django.urls import resolve, reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages

from .models import Book

def index(request): 
    return render(request, 'book.html')

@csrf_exempt
def like(request): 
	if request.method == 'POST':
		# Get raw data from post request
	    id = request.POST['id']
	    title = request.POST['title']
	    authors = request.POST['authors']

	    bookModel, status = Book.objects.get_or_create(api_id=id, title=title, authors=authors)
	    if status: 
	        bookModel.save()
	    else:
	        bucket = Book(api_id=id, title=bookModel.title, authors=bookModel.authors, likes_count=bookModel.likes_count+1)
	        bucket.save()

	    return JsonResponse(bookModel.as_dict())

def leaderboard(request):
	# Take only the first 5
	leaderboards = [bookModel.as_dict() for bookModel in Book.objects.order_by('-likes_count')[:5]]

	return JsonResponse({'leaderboards' : leaderboards})
