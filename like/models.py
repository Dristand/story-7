from django.db import models

# Create your models here.
class Book(models.Model):
    api_id = models.CharField(max_length=20, primary_key=True)
    title = models.CharField(max_length=100)
    authors = models.CharField(max_length=100)
    likes_count = models.IntegerField(default=1)

    def as_dict(self):
        return {
        "title": self.title,
        "authors": self.authors,
        "likes_count": self.likes_count,
        }

    def __str__(self):
        return self.title
