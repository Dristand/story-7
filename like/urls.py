from django.urls import path
from . import views

app_name = 'like'

urlpatterns = [
    path('', views.index, name='index'),
    path('like/', views.like, name='like'),
    path('leaderboard/', views.leaderboard, name='leaderboard')
]