from django.test import TestCase, LiveServerTestCase
from django.urls import resolve, reverse

from .views import index, leaderboard, like
from .models import Book

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import time

# UnitTest
class IndexTest(TestCase):

    def test_root_url_resolves(self):
        found = resolve(reverse('like:index'))
        self.assertEqual(found.func, index)
    
    def test_index_page_returns_correct_html(self):
        response = self.client.get(reverse('like:index'))
        html = response.content.decode('utf8')
        self.assertIn('Google Book API', html)
        self.assertTemplateUsed(response, 'book.html')

    def test_like_url_resolves(self):
        found = resolve(reverse('like:like'))
        self.assertEqual(found.func, like)

    def test_top5_url_resolves(self):
        found = resolve(reverse('like:leaderboard'))
        self.assertEqual(found.func, leaderboard)

class BookModelTest(TestCase):
    def test_book_like(self):
            book1 = Book(api_id="1", title="Bruh", authors="moment")
            book1.save()

            book2 = Book(api_id="2", title="Ayoo", authors=":)")
            book2.save()

            saved_books = Book.objects.all()
            self.assertEqual(saved_books.count(), 2)

            first_book = saved_books[0]
            second_book = saved_books[1]
            self.assertEqual(first_book.title, 'Bruh')
            self.assertEqual(second_book.title, 'Ayoo')

    def test_str_message(self):
        book1 = Book(api_id="1", title="Bruh", authors="moment")
        book1.save()

        self.assertEqual(str(book1), 'Bruh')

# Functional Test
class LikeBook(LiveServerTestCase): 
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
            
    def tearDown(self): 
        self.browser.quit()

    def test_open_page(self): 
        # Bro heard story abt my api website, and decided to take a look
        self.browser.get(self.live_server_url + '/book/')
        self.browser.implicitly_wait(10)

        # Ayoo cool title :D
        self.assertIn('Google Book API', self.browser.title)

        # Search box? Challenge accepted.
        search = self.browser.find_element_by_id('search')
        searchbutton = self.browser.find_element_by_id('searchButton')
        self.assertEqual(
            search.get_attribute('placeholder'),
            'Book Title'
        )

        # He searchs up his fav word : "Ayo"! hehehe
        search.send_keys('Ayo')
        searchbutton.click()

        # Ayooo, there's a like button???
        time.sleep(3)
        like = self.browser.find_element_by_class_name('likeButton')
        like.click()

        # MOARRR LIKE
        like.click()

        # MOARRR BOOK TITLE AYOO
        search.send_keys('More')
        searchbutton.click()
         
        # Me Likey likey
        like = self.browser.find_element_by_class_name('likeButton')
        for i in range (10):
            like.click()
            time.sleep(.5)

        # Bruh, top leaderboard? lets check it out
        leaderboard = self.browser.find_element_by_id('topLeaderBoard')
        self.assertIn(
            'btn',
            leaderboard.get_attribute('class')            
        )
        leaderboard.click()

        # A MODAL, WHY AM I NOT SURPRISED?
        time.sleep(1)
        modal = self.browser.find_element_by_id('contentModal')
        time.sleep(10)
        totalLikes = modal.find_element_by_id('likes')
        self.assertEqual('10', totalLikes.text)

        # He is done, Jeremy is way cooler than he thought he'd be 8-)
