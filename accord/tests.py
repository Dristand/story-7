from django.test import TestCase, LiveServerTestCase
from django.urls import resolve, reverse
from django.http import HttpRequest

from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
import time

# Create your tests here.
class IndexPageTest(TestCase):

    def test_root_url_resolves_to_index_view(self):
        found = resolve(reverse('accord:index'))
        self.assertEqual(found.func, index)

    def test_page_returns_correct_html(self):
        response = self.client.get(reverse('accord:index'))
        html = response.content.decode('utf8')
        self.assertIn('QasQus Accord', html)
        self.assertTemplateUsed(response, 'accord.html')

# FUNCTIONAL TEST

class QasQusBoard(LiveServerTestCase):
    # setUp Test Case
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        super(QasQusBoard, self).setUp()

    # tearDown Test Case
    def tearDown(self):
        self.browser.quit()
        super(QasQusBoard, self).tearDown()

    # test if we can open the page
    def test_can_open_index_page(self):
        # Open the index page
        self.browser.get('%s%s' % (self.live_server_url, '/accord/'))
        self.browser.implicitly_wait(8)

        # Check page title
        self.assertIn('QasQus Accord', self.browser.title)

        # Try to click the first and second accord
        accord1 = self.browser.find_element_by_class_name('a1')
        time.sleep(.5)
        accord1.click()

        header = accord1.find_element_by_class_name('header')
        self.assertIn('active', header.get_attribute('class'))

        # Theres up and down written here, lets try it out
        accord2 = self.browser.find_element_by_class_name('a2')
        itemWrap = self.browser.find_elements_by_class_name('item')
        self.assertEqual('item a1', itemWrap[0].get_attribute('class'))

        # Let's click the down from the first accord
        moveDown = self.browser.find_elements_by_class_name('move-down')
        moveDown[0].click()

        # Wait, it switched with the one below it ? cool.
        itemWrap = self.browser.find_elements_by_class_name('item')
        self.assertEqual('item a2', itemWrap[0].get_attribute('class'))

        # Let's click the up from the second accord
        moveUp = self.browser.find_elements_by_class_name('move-up')
        moveUp[0].click()

        # Woa :O, it really switched place with the upper onee, cool.
        itemWrap = self.browser.find_elements_by_class_name('item')
        self.assertEqual('item a2', itemWrap[0].get_attribute('class'))

#         Challenge Functional Test
        # Lets open it again :D
        body = self.browser.find_element_by_tag_name("html")
        accordElement = self.browser.find_element_by_class_name("accord")
        self.assertNotIn('changeColor', body.get_attribute('class'))
        self.assertNotIn('comicFont', accordElement.get_attribute('class'))

        # Woah a button that said spongebob, lets click it
        spongeButton = self.browser.find_element_by_id('colorChange')
        spongeButton.click()
        time.sleep(10)
        body = self.browser.find_element_by_tag_name("body")

        # MULTIPLE MOCK SPONGEBOB APPEARED, HAHAHAHA
        # the body suddenly have a class which made this happens.
        self.assertIn('changeColor', body.get_attribute('class'))

        # Woah another button saying change style, lets click it (again)
        styleButton = self.browser.find_element_by_id('styleChange')
        styleButton.click()
        time.sleep(10)
        body = self.browser.find_element_by_tag_name("body")

        # Woah the accordion's font changed nicee
        # the class comicFont must be the one who caused it
        self.assertIn('comicFont', accordElement.get_attribute('class'))

        # Ohh that was cool, but enough internet for today. Lets watch some movie :D