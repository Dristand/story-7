from django.test import TestCase
from django.urls import resolve, reverse

from .views import index, signup, dashboard
from django.contrib.auth.views import LoginView, LogoutView 

from django.contrib.auth.models import User

from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import time

class UnitTest(TestCase):

    def test_root_url_resolves_to_index_view(self):
        found = resolve(reverse('authApp:index'))
        self.assertEqual(found.func, index)
    
    def test_index_page_returns_correct_html(self):
        response = self.client.get(reverse('authApp:index'))
        html = response.content.decode('utf8')
        self.assertIn('Auth App', html)
        self.assertTemplateUsed(response, 'auth.html')
    
    def test_login_page_returns_correct_html(self):
        response = self.client.get(reverse('authApp:login'))
        html = response.content.decode('utf8')
        self.assertIn('In', html)
        self.assertTemplateUsed(response, 'login.html')
    
    def test_logout_page_returns_correct_html(self):
        response = self.client.get(reverse('authApp:logout'))
        html = response.content.decode('utf8')
        self.assertIn('Logged out!', html)
        self.assertTemplateUsed(response, 'logout.html')

    def test_signup_url_resolves_to_signup_view(self):
        found = resolve(reverse('authApp:signup'))
        self.assertEqual(found.func, signup)
    
    def test_signup_page_returns_correct_html(self):
        response = self.client.get(reverse('authApp:signup'))
        html = response.content.decode('utf8')
        self.assertIn('Sign Up', html)
        self.assertTemplateUsed(response, 'signup.html')

    def test_user_url_resolves_to_user_view(self):
        found = resolve(reverse('authApp:dashboard'))
        self.assertEqual(found.func, dashboard)


class UserModelTest(TestCase):
    
    def user_creation_test(self):
        users = User.objects.all()
        userNow = users.count()

        response = self.client.post(reverse('authApp:signup'), data={
            'username': 'ayo',
            'password1': 'kitabisa123',
            'password2': 'kitabisa123'
        })

        self.assertEqual(users.count(), userNow + 1)
        last_registered = users[users.count()-1]

        self.assertEqual(last_registered.username, 'ayo')


# Funtional Test

class AuthFunctional(LiveServerTestCase): 
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
            
    def tearDown(self): 
        self.browser.quit()

    def test_can_login(self): 
        # Cap found jeremy's last project and tried to access it
        self.browser.get(self.live_server_url + '/auth')
        self.browser.implicitly_wait(10)

        # Sign Up button? sound the right one cuz i havent make one yet
        self.assertIn('Auth App', self.browser.title)
        signupButton = self.browser.find_element_by_id('signup')
        self.assertEqual(
            signupButton.get_attribute('type'),
            'button'
        )
        signupButton.click()
        
        # Cap sees credential like username, password, and reenter, lets try it
        self.assertIn('Auth App', self.browser.title)

        unameField = self.browser.find_element_by_id('id_username')
        pass1Field = self.browser.find_element_by_id('id_password1')
        pass2Field = self.browser.find_element_by_id('id_password2')

        self.assertEqual(
            unameField.get_attribute('name'),
            'username'
        )

        # Cap tries to cheer jeremy's up :)
        unameField.send_keys('ayo')
        pass1Field.send_keys('kitabisa123')
        pass2Field.send_keys('kitabisa123')

        # cap clicked signup button
        signupButton = self.browser.find_element_by_id('signup-button')
        signupButton.click()
        time.sleep(2)

        # redirected to login page, interesting idea jeremy!
        unameField = self.browser.find_element_by_id('id_username')
        passField = self.browser.find_element_by_id('id_password')

        self.assertEqual(
            passField.get_attribute('name'),
            'password'
        )

        # cap tries to log in
        unameField.send_keys('ayo')
        passField.send_keys('kitabisa123')

        # link starto, eh wrong log in!
        loginButton = self.browser.find_element_by_id('login-button')
        loginButton.click()

        # My username! noice :)
        usname = self.browser.find_element_by_id('usernameP')
        self.assertIn('ayo', usname.text)

        # Enough of this, time to log in to eve online :D
