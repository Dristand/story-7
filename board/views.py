from django.shortcuts import render, redirect

from .models import Message, MessageForm

# Form and all MSG.
def index(request):
    msg = Message.objects.all()
    form = MessageForm()

    return render(request, 'index.html', {'msg' : msg, 'form' : form})

def confirm(request, index):
    msg = Message.objects.all()

    if request.method == 'POST':
            name = request.POST['name']
            text = request.POST['text']

            bucket = Message(name = name, text = text)

            if index == 1:
                return render(request, 'index.html', {'msg' : msg, 'bucket' : bucket})

            elif index == 2:
                bucket.save()

    return redirect('board:index')