from django.test import TestCase, LiveServerTestCase
from django.urls import resolve, reverse
from django.http import HttpRequest

from .views import index, confirm
from .models import Message

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
import time

# Create your tests here.
class IndexPageTest(TestCase):

    def test_root_url_resolves_to_index_view(self):
        found = resolve(reverse('board:index'))
        self.assertEqual(found.func, index)

    def test_page_returns_correct_html(self):
        response = self.client.get(reverse('board:index'))
        html = response.content.decode('utf8')
        self.assertIn('QasQus Board', html)
        self.assertTemplateUsed(response, 'index.html')

    def test_saves_message(self):
        self.client.get(reverse('board:index'))
        self.assertEqual(Message.objects.count(), 0)

    def test_redirects_after_POST(self):
        response = self.client.post(reverse('board:confirm', args=[1]), data={
            'name': 'OwO',
            'text': 'UwU'
        })
        self.assertContains(response, "Author", 1, 200)

class ConfirmPageTest(TestCase):

    def test_confirm_url_resolves_to_correct_view(self):
        found = resolve(reverse('board:confirm', args=[1]))
        self.assertEqual(found.func, confirm)

    def test_page_returns_correct_html(self):
        response = self.client.post(reverse('board:confirm', args=[1]), data={
            'name': 'OwO',
            'text': 'UwU'
        })
        html = response.content.decode('utf8')
        self.assertIn('Confirm', html)
        self.assertTemplateUsed(response, 'index.html')

    def test_if_displays_bucket_message_to_confirm(self):
        response = self.client.post(reverse('board:confirm', args=[1]), data={
            'name': 'OwO',
            'text': 'UwU'
        })
        self.assertContains(response, 'OwO', 2, 200)
        self.assertContains(response, 'UwU', 2, 200)

    def test_confirm_save_bucket_message(self):
        response = self.client.post(reverse('board:confirm', args=[2]), data={
            'name': 'OwO',
            'text': 'UwU'
        })
        bucket = Message.objects.all()
        self.assertEqual(bucket.count(), 1)


    def test_confirm_cancel_recent_msg(self):
        response = self.client.post(reverse('board:confirm', args=[3]), data={
            'name': 'OwO',
            'text': 'UwU'
        })
        bucket = Message.objects.all()
        self.assertEqual(bucket.count(), 0)


class TestMessage(TestCase):

    def test_saving_and_get_saved_message(self):
        msg1 = Message(name='Owo', text = 'OWO')
        msg1.save()

        msg2 = Message(name='Uwu', text = 'UWU')
        msg2.save()

        bucket = Message.objects.all()
        self.assertEqual(bucket.count(), 2)

        first_msg = bucket[0]
        second_msg = bucket[1]
        self.assertEqual(first_msg.text, 'OWO')
        self.assertEqual(first_msg.name, 'Owo')

        self.assertEqual(second_msg.text, 'UWU')
        self.assertEqual(second_msg.name, 'Uwu')

    def test_str_message(self):
        msg1 = Message(name='Owo', text = 'OWO')
        msg1.save()

        self.assertEqual(str(msg1), 'Owo')


# FUNCTIONAL TEST

class QasQusBoard(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        super(QasQusBoard, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(QasQusBoard, self).tearDown()

    def test_can_open_index_page(self):
        # Open the index page
        self.browser.get(self.live_server_url)
        self.browser.implicitly_wait(8)

        # Check page title
        self.assertIn('QasQus Board', self.browser.title)

        # Form Test
        form_name = self.browser.find_element_by_id('id_name')
        form_text = self.browser.find_element_by_id('id_text')

        # Check input
        form_name.send_keys('Owo')
        form_text.send_keys('OWO')

        # Submit input data
        form_name.send_keys(Keys.ENTER)
        self.browser.implicitly_wait(10)
        self.assertIn('QasQus Board', self.browser.title)

        # Try cancel the answer
        cancel = self.browser.find_element_by_id('cancel')
        cancel.click()

        # check for the submitted text
        self.browser.implicitly_wait(10)
        self.assertNotIn('OWO', self.browser.page_source)

    def test_cancel_submit(self):
        # Open the index page
        self.browser.get(self.live_server_url)
        self.browser.implicitly_wait(8)

        # Check page title
        self.assertIn('QasQus Board', self.browser.title)

        # Form Test
        form_name = self.browser.find_element_by_id('id_name')
        form_text = self.browser.find_element_by_id('id_text')

        # Check input
        form_name.send_keys('Uwu')
        form_text.send_keys('UWU')

        # Submit input data
        form_name.send_keys(Keys.ENTER)
        self.browser.implicitly_wait(10)
        self.assertIn('QasQus Board', self.browser.title)

        # Try confirm the answer
        cancel = self.browser.find_element_by_id('confirm')
        cancel.click()

        # check for the submitted text
        self.browser.implicitly_wait(10)
        self.assertIn('UWU', self.browser.page_source)