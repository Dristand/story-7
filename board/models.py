from django.db import models
from django import forms
from django.forms import ModelForm

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length = 255)
    text = models.TextField()

    def __str__(self):
        return self.name

class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = '__all__'
